#!/bin/bash

filename='/srv/config/hosts.list'

## Loop through all sites
#for ((i=0; i < ${#DOMAINS[@]}; i++)); do
while IFS='' read -r line || [[ -n "$line" ]]; do

    ## Current Domain
    DOMAIN=${line}
    set -- "$DOMAIN" 
    IFS=" | "; declare -a DOMAIN=($*)

    # If ${DOMAIN[0]} dir already exists we skip it and continue with next {DOMAIN[0]}
    if [ -d "/var/www/${DOMAIN[0]}" ]; then
        echo "Site ${DOMAIN[0]} exists; skip creation"
        continue
    fi

    mkdir -p "/var/www/${DOMAIN[0]}"
    cd /var/www/${DOMAIN[0]}

    # Create local-config.php for Site
    DBNAME=$( echo ${DOMAIN[0]} | sed -e 's/[^a-zA-Z0-9]/_/g')   

    echo "Creating vhost config for ${DOMAIN[0]}..."
    sudo cp /etc/apache2/sites-available/rockschtar.box.local.conf /etc/apache2/sites-available/${DOMAIN[0]}.conf

    echo "Updating vhost config for ${DOMAIN[0]}..."
    sudo sed -i s,scotchbox.local,${DOMAIN[0]},g /etc/apache2/sites-available/${DOMAIN[0]}.conf
    sudo sed -i s,/var/www/public,/var/www/${DOMAIN[0]},g /etc/apache2/sites-available/${DOMAIN[0]}.conf

    echo "Enabling ${DOMAIN[0]}. Will probably tell you to restart Apache..."
    sudo a2ensite ${DOMAIN[0]}.conf

done < "$filename"

echo "So let's restart apache..."
sudo service apache2 restart
